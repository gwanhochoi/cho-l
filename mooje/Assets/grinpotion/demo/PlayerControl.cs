﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {
    private Animator animController;

    void Awake()
    {
        animController = GetComponent<Animator>();
    }

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            animController.SetTrigger("press_button");
        }
	}
}
