﻿using UnityEngine;
using System.Collections;

public class PlayerCon : Photon.MonoBehaviour {
	public bool isControllable;
	public string Status;
	public Vector2 aniDirection;
	private Vector3 pos;
	private float Speed;
	private Touch touch;
	private Vector3 moveVec;
	private float halfScreenWidth;
	private float halfScreenHeight;
	private Animator animator;
	private bool moving = false;
	private VirtualPad vpad;
	public enum aniStatus{
		idle, walk, attack
	}
	private aniStatus ast;
		// Use this for initialization
	void Awake () {
		vpad = GameObject.FindObjectOfType<VirtualPad> ();
		ast = aniStatus.idle;
		animator = GetComponent<Animator> ();

		Speed = 2.0f;
		halfScreenWidth = Screen.width / 2;
		halfScreenHeight = Screen.height / 2;
		aniDirection = new Vector2 (0, 0);

	}

	void Update(){
		if (isControllable) {
			float vpad_vertical = 0.0f;
			float vpad_horizontal = 0.0f;
			zFOXVPAD_BUTTON vpad_btnAttack = zFOXVPAD_BUTTON.NON;
			if (vpad != null) {
				vpad_vertical = vpad.vertical;
				vpad_horizontal = vpad.horizontal;
				vpad_btnAttack = vpad.ButtonAttack;
			}

			float vpadMvh = vpad_horizontal;
			float vpadMvv = vpad_vertical;

			vpadMvh = Mathf.Pow(Mathf.Abs(vpadMvh), 1.5f) * Mathf.Sign(vpadMvh);
			vpadMvv = Mathf.Pow(Mathf.Abs(vpadMvv), 1.5f) * Mathf.Sign(vpadMvv);

			PlayerMove(vpadMvh, vpadMvv);
				/*
				pos = Input.GetTouch(0).position;
				moveVec.x = pos.x - halfScreenWidth;
				moveVec.y = pos.y - halfScreenHeight;
				moveVec.z = 0.0f;
				if(moveVec.x > 0){
					aniDirection = new Vector2(0, 0);
				}else{
					aniDirection = new Vector2(0, 180);
					moveVec.x *= -1;
				}
				
				if(Input.GetTouch(0).phase == TouchPhase.Began){
					transform.eulerAngles = aniDirection;
					moving = !moving;
					if(moving == true){
						ast = aniStatus.walk;
					}else
						ast = aniStatus.idle;
				}*/




		}
	}

	void PlayerMove(float h, float v){
		transform.Translate (new Vector3 (h, v, 0.0f) * Time.deltaTime * Speed);
	}

	void OnTriggerEnter2D(Collider2D other){

	}
}
