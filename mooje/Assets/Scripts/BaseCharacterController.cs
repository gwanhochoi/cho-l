﻿using UnityEngine;
using System.Collections;


public class BaseCharacterController : Photon.MonoBehaviour {
	public Animator animator;
	protected float mvSpeed = 2.0f;
	protected Vector3 moveDir = Vector3.zero;
	public bool isControllable = false;
	public float aniDir = 0;
	public Health health;

	// Use this for initialization
	protected virtual void Awake () {
		animator = GetComponent<Animator> ();
		health = GetComponent<Health> ();
	}
	
	// Update is called once per frame
	protected virtual void FixedUpdate () {
		FixedUpdateCharacter ();
		if (isControllable) {
			transform.Translate (moveDir * Time.deltaTime * mvSpeed);

		}




	}

	protected virtual void FixedUpdateCharacter(){

	}

	void OnTriggerEnter2D(Collider2D other){
		
		if (photonView.isMine) {
			if (other.tag == "AragonAttack") {
				//if(photonView.isMine)
				PhotonNetwork.LeaveRoom();
			}
			if(other.tag == "Player"){
				int level = other.GetComponent<Health>().playerLevel;
				if (level - health.playerLevel < -2.0f) {
					//other.GetComponent<PlayerController>().Life = false;
					//Life = false;
					//Destroy(gameObject);
					//PhotonNetwork.LeaveRoom();
					photonView.RPC("Big", PhotonTargets.All, level);
					
					//I die
				}else if(level - health.playerLevel > 2.0f){
					//PhotonNetwork.Destroy(gameObject);
					//StartCoroutine(Die());
				}
			}
		}
		
	}

}
