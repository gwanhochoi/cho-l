﻿using UnityEngine;
using System.Collections;

public enum SelectHero{
	Aragon, Dalf, Robin,
}

public class LobbyController : MonoBehaviour {

	int aragonButtonIndex = -1;
	int dalfButtonIndex = -1;
	int robinButtonIndex = -1;

	bool aragonButtonHit = false;
	bool dalfButtonHit = false;
	bool robinButtonHit = false;
	bool startButtonHit = false;

	Animator animAragon;
	Animator animDalf;
	Animator animRobin;

	Transform tfStartButton;

	public zFOXVPAD_BUTTON AragonButton = zFOXVPAD_BUTTON.NON;
	public zFOXVPAD_BUTTON DalfButton = zFOXVPAD_BUTTON.NON;
	public zFOXVPAD_BUTTON RobinButton = zFOXVPAD_BUTTON.NON;
	public zFOXVPAD_BUTTON StartButton = zFOXVPAD_BUTTON.NON;

	// Use this for initialization
	void Awake () {
		if (!PhotonNetwork.connected) {
			PhotonNetwork.ConnectUsingSettings("1.0");
		}
		Screen.orientation = ScreenOrientation.Landscape;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		Screen.SetResolution (Screen.width, Screen.width / 16 * 10, true);
		animAragon = GameObject.Find ("warrior").GetComponent<Animator>();
		animDalf = GameObject.Find ("wizard").GetComponent<Animator>();
		animRobin = GameObject.Find ("archer").GetComponent<Animator>();

		tfStartButton = GameObject.Find ("StartButton").transform;

	}
	
	// Update is called once per frame
	void Update () {

		if (AragonButton == zFOXVPAD_BUTTON.UP) {
			AragonButton = zFOXVPAD_BUTTON.NON;
			aragonButtonIndex = -1;
		}
		if (DalfButton == zFOXVPAD_BUTTON.UP) {
			DalfButton = zFOXVPAD_BUTTON.NON;
			dalfButtonIndex = -1;
		}
		if (RobinButton == zFOXVPAD_BUTTON.UP) {
			RobinButton = zFOXVPAD_BUTTON.NON;
			robinButtonIndex = -1;
		}

		aragonButtonHit = false;
		dalfButtonHit = false;
		robinButtonHit = false;
		startButtonHit = false;

		if (Input.touchCount > 0) {
			bool objectTouched = false;
			Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
			RaycastHit hit;
				
			if (Physics.Raycast (ray, out hit, Mathf.Infinity)) {
				TouchPhase tp = Input.GetTouch (0).phase;
				if (tp == TouchPhase.Began) {
					CheckButtonDown (hit);
					objectTouched = true;
				} else if (tp == TouchPhase.Ended || tp == TouchPhase.Canceled) {
					CheckButtonUp (hit);
					objectTouched = true;
				}
			}
			if (!objectTouched) {
				CheckButtonNon ();
			}
		}
	}

	void CheckButtonDown(RaycastHit hit){
		if (hit.collider.gameObject == animAragon.gameObject) {
			AragonButton = zFOXVPAD_BUTTON.DOWN;
			aragonButtonHit = true;
			animAragon.SetFloat ("MovSpeed", 0.2f);
			animDalf.SetFloat ("MovSpeed", 0.0f);
			animRobin.SetFloat ("MovSpeed", 0.0f);
		}//else if()   ----> add button
		else if (hit.collider.gameObject == animDalf.gameObject) {
			DalfButton = zFOXVPAD_BUTTON.DOWN;
			dalfButtonHit = true;
			animAragon.SetFloat ("MovSpeed", 0.0f);
			animDalf.SetFloat ("MovSpeed", 0.2f);
			animRobin.SetFloat ("MovSpeed", 0.0f);
		} else if (hit.collider.gameObject == animRobin.gameObject) {
			RobinButton = zFOXVPAD_BUTTON.DOWN;
			robinButtonHit = true;
			animAragon.SetFloat ("MovSpeed", 0.0f);
			animDalf.SetFloat ("MovSpeed", 0.0f);
			animRobin.SetFloat ("MovSpeed", 0.2f);
		} else if (hit.collider.gameObject == tfStartButton.gameObject) {
			PhotonNetwork.JoinRandomRoom();
			StartButton = zFOXVPAD_BUTTON.DOWN;
			startButtonHit = true;
			animAragon.SetFloat ("MovSpeed", 0.0f);
			animDalf.SetFloat ("MovSpeed", 0.0f);
			animRobin.SetFloat ("MovSpeed", 0.0f);
		}
	}

	void CheckButtonUp(RaycastHit hit){
		if (hit.collider.gameObject == animAragon.gameObject) {
			AragonButton = zFOXVPAD_BUTTON.UP;
		} else if (hit.collider.gameObject == animDalf.gameObject) {
			DalfButton = zFOXVPAD_BUTTON.UP;
		} else if (hit.collider.gameObject == animRobin.gameObject) {
			RobinButton = zFOXVPAD_BUTTON.UP;
		}
	}

	void CheckButtonNon(){
		if (!aragonButtonHit) {
			AragonButton = zFOXVPAD_BUTTON.NON;
			aragonButtonIndex = -1;
		}
		if (!dalfButtonHit) {
			DalfButton = zFOXVPAD_BUTTON.NON;
			dalfButtonIndex = -1;
		}
		if (!robinButtonHit) {
			RobinButton = zFOXVPAD_BUTTON.NON;
			robinButtonIndex = -1;
		}
	}

	private IEnumerator MoveToGameScene(){
		//PhotonNetwork.room -> Get the room we're currently in. Null if we aren't in any room
		while (PhotonNetwork.room == null) {
			yield return 0;
		}
		
		PhotonNetwork.isMessageQueueRunning = false;
		Application.LoadLevel (1);
		
	}

	private void OnJoinedRoom(){
		this.StartCoroutine (this.MoveToGameScene ());
	}
	
	private void OnCreatedRoom(){
		this.StartCoroutine (this.MoveToGameScene ());
	}

	void OnPhotonRandomJoinFailed()
	{
		PhotonNetwork.CreateRoom(null, true, true, 20); 
	}

	void OnFailedToConnectToPhoton(DisconnectCause cause){
		PhotonNetwork.ConnectUsingSettings("1.0");
	}
}
