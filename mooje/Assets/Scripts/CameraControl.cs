﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {
	private GameObject cam;
	private PhotonView pv;
	void Awake(){
		cam = GameObject.Find("GameCamera");
		pv = GetComponent<PhotonView> ();
	}

	// Update is called once per frame
	void Update () {
		if (pv.isMine == true) {
			Vector3 v = transform.position;
			v.z = -10.0f;
			cam.transform.position = v;
		}

	}
}
