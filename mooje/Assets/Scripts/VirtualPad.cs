﻿using UnityEngine;
using System.Collections;

public enum zFOXVPAD_BUTTON{
	NON,
	DOWN,
	HOLD,
	UP,
}

public enum zFOXVPAD_SLIDEPADVALUEMODE{
	PAD_XY_SCREEN_WH,
	PAD_XY_SCREEN_WW,
	PAD_XY_SCREEN_HH,
}

public class VirtualPad : MonoBehaviour {
	public float padSensitive = 25.0f;

	public zFOXVPAD_SLIDEPADVALUEMODE padValMode = 
		zFOXVPAD_SLIDEPADVALUEMODE.PAD_XY_SCREEN_WW;
	public float horizontalStartVal = 0.05f;
	public float verticalStartVal = 0.05f;

	public bool autoLayout = false;
	public bool autoLayoutUpdate = false;
	public Vector2 autoLayoutPos_SlidePad = new Vector2 (0.7f, 0.5f);
	public Vector2 autoLayoutPos_ButtonAttack = new Vector2 (0.5f, 0.5f);

	[Header("--- Debug ---")]
	public float horizontal = 0.0f;
	public float vertical = 0.0f;

	public zFOXVPAD_BUTTON ButtonAttack = zFOXVPAD_BUTTON.NON;


	Camera uicam;
	SpriteRenderer sprSlidePad;
	SpriteRenderer sprSlidePadBack;
	SpriteRenderer sprButtonAttack;

	int buttonAttackIndex = -1;
	bool buttonAttackHit = false;

	bool movPadEnable = false;
	Vector2 movSt = Vector2.zero;
	Vector2 mov = Vector2.zero;
	bool movEnable = false;

	void Awake(){
		uicam = GameObject.Find ("FUIPadCamera").GetComponent<Camera> () as Camera;
		sprButtonAttack = GameObject.Find ("ButtonAttack").GetComponent<SpriteRenderer> () as SpriteRenderer;
		sprSlidePadBack = GameObject.Find ("SlidePadBack").GetComponent<SpriteRenderer>() as SpriteRenderer;
		sprSlidePad = GameObject.Find ("SlidePad").GetComponent<SpriteRenderer>() as SpriteRenderer;


		RunAutoLayout ();
	}

	void RunAutoLayout(){
		if (autoLayout) {
			Vector3 scPos = uicam.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0.0f)) - uicam.transform.position;
			Vector3 posPad = new Vector3(-scPos.x * autoLayoutPos_SlidePad.x, -scPos.y * autoLayoutPos_SlidePad.y, 0.0f);
			sprSlidePadBack.transform.localPosition = posPad;
			Vector3 posBtnA = new Vector3(scPos.x * autoLayoutPos_ButtonAttack.x, -scPos.y * autoLayoutPos_ButtonAttack.y, 0.0f);
			sprButtonAttack.transform.localPosition = posBtnA;

		}
	}

	void Update(){
		if (autoLayoutUpdate) {
			RunAutoLayout();
		}

		if (ButtonAttack == zFOXVPAD_BUTTON.UP) {
			ButtonAttack = zFOXVPAD_BUTTON.NON;
			buttonAttackIndex = -1;
		}

		buttonAttackHit = false;

		if (Input.touchCount > 0) {
			bool objectTouched = false;
			for (int i=0; i<Input.touchCount; i++) {
				Ray ray = uicam.ScreenPointToRay (Input.GetTouch (i).position);
				RaycastHit hit;

				if (Physics.Raycast (ray, out hit, Mathf.Infinity)) {
					TouchPhase tp = Input.GetTouch (i).phase;
					if (tp == TouchPhase.Began) {
						CheckButtonDown (hit, i);
						objectTouched = true;
					} else if (tp == TouchPhase.Moved || tp == TouchPhase.Stationary) {
						CheckButtonMove (hit, i);
						objectTouched = true;
					} else if (tp == TouchPhase.Ended || tp == TouchPhase.Canceled) {
						CheckButtonUp (hit, i);
						objectTouched = true;
					}
				}
			}
			if (!objectTouched) {
				CheckButtonNon ();
			}
		}

		movEnable = false;
		if (Input.touchCount > 0) {
			for(int i=0; i<Input.touchCount; i++){
				if(i != buttonAttackIndex){
					TouchPhase tp = Input.GetTouch(i).phase;
					if(tp == TouchPhase.Began){
						if(CheckSlidePadDown(Input.GetTouch(i).position)){
							break;
						}
					} else if(tp == TouchPhase.Moved || tp == TouchPhase.Stationary){
						if(CheckSlidePadMove(Input.GetTouch(i).position)){
							break;
						}
					}else if(tp == TouchPhase.Ended || tp == TouchPhase.Canceled){
						CheckSlidePadUp();
					}
				}
			}
		}

		if (movEnable == false) {
			movPadEnable = false;
			mov = Vector2.zero;
		}

		switch (padValMode) {
		case zFOXVPAD_SLIDEPADVALUEMODE.PAD_XY_SCREEN_WH:
			horizontal = mov.x * padSensitive / Screen.width;
			vertical = mov.y * padSensitive / Screen.height;
			break;

		case zFOXVPAD_SLIDEPADVALUEMODE.PAD_XY_SCREEN_WW:
			horizontal = mov.x * padSensitive / Screen.width;
			vertical = mov.y * padSensitive / Screen.width;
			break;

		case zFOXVPAD_SLIDEPADVALUEMODE.PAD_XY_SCREEN_HH:
			horizontal = mov.x * padSensitive / Screen.height;
			vertical = mov.y * padSensitive / Screen.height;
			break;
		}

		if (horizontal < -1.0f)
			horizontal = -1.0f;
		if (horizontal > 1.0f)
			horizontal = 1.0f;
		if (vertical < -1.0f)
			vertical = -1.0f;
		if (vertical > 1.0f)
			vertical = 1.0f;

		if (Mathf.Abs (horizontal) < horizontalStartVal) {
			horizontal = 0.0f;
		}

		if (Mathf.Abs (vertical) < verticalStartVal) {
			vertical = 0.0f;
		}

		Vector3 pos = new Vector3 (horizontal / (padSensitive / 2.0f), vertical / (padSensitive / 2.0f), 0.0f);
		sprSlidePad.transform.localPosition = pos;

	}

	void CheckButtonDown(RaycastHit hit, int i){
		if (hit.collider.gameObject == sprButtonAttack.gameObject) {
			ButtonAttack = zFOXVPAD_BUTTON.DOWN;
			buttonAttackIndex = i;
			buttonAttackHit = true;
			sprButtonAttack.color = new Color(1.0f, 0.0f, 0.0f);
		}//else if()   ----> add button
	}

	void CheckButtonMove(RaycastHit hit, int i){
		if (hit.collider.gameObject == sprButtonAttack.gameObject) {
			ButtonAttack = zFOXVPAD_BUTTON.HOLD;
			buttonAttackIndex = i;
			buttonAttackHit = true;
		}
	}

	void CheckButtonUp(RaycastHit hit, int i){
		if (hit.collider.gameObject == sprButtonAttack.gameObject) {
			ButtonAttack = zFOXVPAD_BUTTON.UP;
			buttonAttackIndex = i;
			sprButtonAttack.color = new Color(1.0f, 1.0f, 1.0f);
		}
	}

	void CheckButtonNon(){
		if (!buttonAttackHit) {
			ButtonAttack = zFOXVPAD_BUTTON.NON;
			buttonAttackIndex = -1;
			sprButtonAttack.color = new Color(1.0f, 1.0f, 1.0f);
		}
	}

	bool CheckSlidePadDown(Vector2 posTouch){
		if (posTouch.x / Screen.width < 0.5f) {
			movPadEnable = true;
			movEnable = true;
			movSt = posTouch;
			Vector3 vec3 = uicam.ScreenToWorldPoint(posTouch);
			vec3.z = sprSlidePad.transform.position.z;
			sprSlidePadBack.transform.position = vec3;
			return true;
		}

		return false;
	}

	bool CheckSlidePadMove(Vector2 posTouch){
		if (movPadEnable) {
			movEnable = true;
			mov = posTouch - movSt;
			sprSlidePad.color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
			return true;
		}
		return false;
	}

	void CheckSlidePadUp(){
		sprSlidePad.color = new Color (1.0f, 1.0f, 1.0f, 0.2f);
	}






}
