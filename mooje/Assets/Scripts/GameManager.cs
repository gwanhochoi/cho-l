﻿using UnityEngine;
using System.Collections;

public class GameManager : Photon.MonoBehaviour {
	public Transform playerPrefab;
	private GameObject go = null;
	//VirtualPad vpad;

	// Use this for initialization
	void Awake () {
		Screen.orientation = ScreenOrientation.Landscape;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		//vpad = GameObject.FindObjectOfType<VirtualPad> ();

		PhotonNetwork.isMessageQueueRunning = true;
		if (PhotonNetwork.isMasterClient == false) {

			//PhotonNetwork.Instantiate (this.playerPrefab.name, new Vector3(-3.0f, -6.0f, 0.0f), playerPrefab.rotation, 0);
			go = PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(-3.0f, -6.0f, 0.0f), playerPrefab.rotation,0);
		} else {
			//PhotonNetwork.Instantiate (this.playerPrefab.name, new Vector3(3.0f, -6.0f, 0.0f), playerPrefab.rotation, 0);
			go = PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(3.0f, -6.0f, 0.0f), playerPrefab.rotation,0);
		}

	
	}



	
	public void OnLeftRoom(){
		Debug.Log ("OnLeftRoom(local)");
		
		Application.LoadLevel (0);
	}
	
	public void OnMasterClientSwitched(PhotonPlayer player){
		Debug.Log ("OnMasterClientSwitched: " + player);
		if (PhotonNetwork.connected) {
			photonView.RPC("SendChatMessage", PhotonNetwork.masterClient, 
			               "Hi master! From : " + PhotonNetwork.player);
			photonView.RPC("SendChatMessage", PhotonTargets.All, "WE GOT A NEW MASTER : " + player + "==" + 
			               PhotonNetwork.masterClient + "From:" + PhotonNetwork.player);
		}
	}
	
	public void OnDisconnectedFromPhoton(){
		Debug.Log ("OnDisconnectedFromPhoton");
		Application.LoadLevel (0);
	}
	
	public void OnPhotonPlayerConnected(PhotonPlayer player){
		Debug.Log ("OnPhotonPlayerConnected" + player);
	}
	
	public void OnPhotonPlayerDisconnected(PhotonPlayer player){
		Debug.Log ("OnPlayerDisConnected" + player);
	}
	
	public void OnReceivedRoomList(){
		Debug.Log ("OnReceivedRoomList");
	}
	
	public void OnReceivedRoomListUpdate(){
		Debug.Log ("OnReceivedRoomListUpdate");
	}
	
	public void OnConnectedToPhoton(){
		Debug.Log ("OnConnectedToPhoton");
	}
	
	public void OnFailedToConnectToPhoton(){
		Debug.Log ("OnFailedToConnectToPhoton");
	}
	
	public void OnPhotonInstantiate(PhotonMessageInfo info){
		Debug.Log ("OnPhotonInstantiate " + info.sender);
	}

	
	
	
	
	
}