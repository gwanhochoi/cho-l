﻿using UnityEngine;
using System.Collections;

public class PlayerMain : MonoBehaviour {
	PlayerController_aragon playerCtrl;
	VirtualPad vpad;
	// Use this for initialization
	void Awake () {
		playerCtrl = GetComponent<PlayerController_aragon> ();
		vpad = GameObject.FindObjectOfType<VirtualPad> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (playerCtrl.isControllable) {
			float vpad_vertical = 0.0f;
			float vpad_horizontal = 0.0f;
			zFOXVPAD_BUTTON vpad_btnAttack = zFOXVPAD_BUTTON.NON;
			if (vpad != null) {
				vpad_vertical = vpad.vertical;
				vpad_horizontal = vpad.horizontal;
				vpad_btnAttack = vpad.ButtonAttack;
			}
			
			float vpadMvh = vpad_horizontal;
			float vpadMvv = vpad_vertical;
			
			vpadMvh = Mathf.Pow(Mathf.Abs(vpadMvh), 1.5f) * Mathf.Sign(vpadMvh);
			vpadMvv = Mathf.Pow(Mathf.Abs(vpadMvv), 1.5f) * Mathf.Sign(vpadMvv);

			//attack
			if(vpad_btnAttack == zFOXVPAD_BUTTON.DOWN){
				playerCtrl.ActionAttack();
				return;
			}


			//move
			playerCtrl.ActionMove (vpadMvh, vpadMvv);
		}

	}
}
