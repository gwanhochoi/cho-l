﻿using UnityEngine;
using System.Collections;

public class PlayerController_aragon: BaseCharacterController {
	public readonly static int ANISTS_ATTACK_A = 
		Animator.StringToHash("Base Layer.aragon_attack");
	// Use this for initialization
	protected override void Awake () {
		base.Awake ();
	}
	
	protected override void FixedUpdateCharacter(){
		AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo (0);
		if (stateInfo.nameHash == ANISTS_ATTACK_A) {
			mvSpeed = 0.0f;
		} else {
			animator.SetBool("Attack_A", false);
			mvSpeed = 2.0f;
		}

			
	}

	public void ActionMove(float h, float v){
		moveDir = Vector3.Normalize (new Vector3 (h, v, 0.0f));
		float aniMovSpeed = Mathf.Abs (moveDir.x) + Mathf.Abs(moveDir.y);
		animator.SetFloat ("MovSpeed", aniMovSpeed);

		if (moveDir.x < 0.0f) {
			transform.eulerAngles = new Vector2 (0, 180);
			aniDir = -1.0f;
			moveDir.x *= -1.0f;
		} else if (moveDir.x > 0.0f) {
			transform.eulerAngles = new Vector2 (0, 0);
			aniDir = 1.0f;
		} else {
			
		}
	}

	public void ActionAttack(){
		animator.SetBool ("Attack_A", true);
		health.playerLevel ++;
		transform.localScale += new Vector3 (0.1f, 0.1f, 0.0f);
	}
	[PunRPC]
	void Big(int level){
		transform.localScale += new Vector3 (level * 0.1f, level * 0.1f, 0.0f);
		health.playerLevel += level;
	}

	IEnumerator Die(){
		yield return new WaitForSeconds (1.5f);
		PhotonNetwork.LeaveRoom ();
	}




}
