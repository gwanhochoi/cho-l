﻿using UnityEngine;
using System.Collections;

public class PlayerNetwork : Photon.MonoBehaviour {
	private PlayerController_aragon playercontrol;
	private Vector3 correctoPos = Vector3.zero;
	private Animator animator;
	private float aniDir = -1.0f;
	void Awake(){
		playercontrol = GetComponent<PlayerController_aragon> ();
		animator = GetComponent<Animator> ();
		if (photonView.isMine) {
			playercontrol.isControllable = true;
		} else {
			playercontrol.isControllable = false;
		}
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
		if (stream.isWriting) {
			stream.SendNext(transform.position);
			stream.SendNext(playercontrol.aniDir);
			stream.SendNext(playercontrol.health.playerLevel);
			stream.SendNext(playercontrol.transform.localScale);
			stream.SendNext(animator.GetFloat("MovSpeed"));
			stream.SendNext(animator.GetBool("Attack_A"));


		} else {
			correctoPos = (Vector3)stream.ReceiveNext();
			aniDir = (float)stream.ReceiveNext();
			playercontrol.health.playerLevel = (int)stream.ReceiveNext();
			playercontrol.transform.localScale = (Vector3)stream.ReceiveNext();
			animator.SetFloat("MovSpeed", (float)stream.ReceiveNext());
			animator.SetBool("Attack_A", (bool)stream.ReceiveNext());

		}
	}

	void Update(){
		if (photonView.isMine == true)
			return;
		float distance = Vector3.Distance (transform.position, this.correctoPos);
		if (distance < 2) {
			transform.position = Vector3.Lerp(transform.position, this.correctoPos, Time.deltaTime *5.0f);
		} else {
			transform.position = this.correctoPos;
		}

		if (aniDir < 0.0f)
			transform.eulerAngles = new Vector2 (0, 180);
		else
			transform.eulerAngles = new Vector2 (0, 0);


	}


}
